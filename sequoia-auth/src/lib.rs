use std::time::Duration;

use sequoia_openpgp as openpgp;
use openpgp::{
    Cert,
    Packet,
    Result,
    armor,
    crypto::Signer,
    packet::signature::{SignatureBuilder, subpacket::NotationDataFlags},
    parse::{Parse, PacketParserResult, PacketParser},
    policy::{HashAlgoSecurity, Policy, StandardPolicy},
    serialize::{Serialize, SerializeInto},
    types::SignatureType,
};

/// Creates an login token.
///
/// `cert` is the identity for logging in.  `signer` is used to create
/// the signature, which must be the signer of a (sub)key of `cert`
/// marked as authentication-capable.
///
/// `hostname` is the name the login token is meant for.
pub fn create_token(cert: Cert, signer: &mut dyn Signer, hostname: &str)
                    -> Result<String>
{
    let sig_builder = SignatureBuilder::new(SignatureType::Standalone)
        .set_signature_validity_period(Duration::new(10, 0))?
        .set_notation(
            "login-hostname@notations.sequoia-pgp.org",
            hostname.as_bytes().to_vec(),
            NotationDataFlags::empty()
                .set_human_readable(),
            true,
        )?
        .set_notation(
            "issuer-cert@notations.sequoia-pgp.org",
            cert.to_vec()?,
            NotationDataFlags::empty(),
            false,
        )?;
    let sig = sig_builder.sign_standalone(signer)?;

    let mut writer = armor::Writer::new(Vec::new(), armor::Kind::Signature)?;
    Packet::from(sig).serialize(&mut writer)?;
    let buf = writer.finalize()?;

    Ok(String::from_utf8(buf)?)
}

/// Authenticates an login token.
pub fn authenticate<T>(token: T, hostname: &str)
                       -> Result<String>
where
    T: AsRef<[u8]>,
{
    let mut p = StandardPolicy::new();
    p.good_critical_notations(&[
        "login-hostname@notations.sequoia-pgp.org",
    ]);

    let mut sig;
    let ppr = PacketParser::from_bytes(token.as_ref())?;
    if let PacketParserResult::Some(pp) = ppr {
        let (packet, _next_ppr) = pp.next()?;
        match packet {
            Packet::Signature(s) => sig = s,
            p => return
                Err(anyhow::anyhow!("Unexpected packet: {:?}", p)),
        }
    } else {
        return Err(anyhow::anyhow!("Expected a signature, got eof"));
    }

    // Check the signature.

    // 1. Check with our policy.
    p.signature(&sig, HashAlgoSecurity::CollisionResistance)?;

    // 2. Make sure the signature is alive.
    sig.signature_alive(None, None)?;

    // 3. Make sure the signature is for us.
    if let Some(login_hostname) =
        sig.notation("login-hostname@notations.sequoia-pgp.org")
        .next()
    {
        if login_hostname == hostname.as_bytes() {
            // Good.
        } else {
            return Err(anyhow::anyhow!("Wrong login hostname found: {}",
                                       String::from_utf8_lossy(login_hostname)));
        }
    } else {
        return Err(anyhow::anyhow!("No login hostname found"));
    };

    let issuer = sig.get_issuers().get(0).cloned().ok_or(
        anyhow::anyhow!("No issuer given"))?;
    let cert = if let Some(cert_bytes) =
        sig.notation("issuer-cert@notations.sequoia-pgp.org")
        .next()
    {
        Cert::from_bytes(&cert_bytes)?
    } else {
        return Err(anyhow::anyhow!("No issuer certificate found"));
    };

    let user = cert.fingerprint().to_string();

    if let Some(signer) = cert.with_policy(&p, None)?
        .keys()
        .for_signing() // XXX: for_authentication
        .alive()
        .revoked(false)
        .key_handle(issuer)
        .next()
    {
        sig.verify_standalone(&signer)?;
    } else {
        return Err(anyhow::anyhow!("Issuer not found or not valid"));
    }

    // Successfully authenticated token.
    Ok(user)
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
