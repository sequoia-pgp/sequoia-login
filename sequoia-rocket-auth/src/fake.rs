/// Hack to create a rocket_auth::Login.

#[derive(serde::Serialize)]
pub struct Login {
    email: String,
    password: String,
}

impl Login {
    pub fn new(user: &str) -> anyhow::Result<rocket_auth::Login> {
        Ok(serde_json::from_slice(&serde_json::to_vec(&Self {
            email: user.into(),
            password: "".into(),
        })?)?)
    }
}
