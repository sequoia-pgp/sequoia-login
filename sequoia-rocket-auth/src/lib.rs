#[macro_use] extern crate rocket;
use rocket::{
    form::Form,
    http::uri::Reference,
    response::{
        content,
        Redirect,
    },
    response::status::Unauthorized,
    State,
};

use rocket_auth::{
    Auth,
};

use sequoia_auth::authenticate;

mod fake;

pub struct Config {
    hostname: String,
    redirect_to: Reference<'static>,
}

impl Config {
    /// Configures authentication.
    ///
    /// `hostname` must be the client-visible host name for this
    /// instance.  `redirect_to` is the relative URL where successful
    /// logins and logouts redirect to.
    pub fn new<H, R>(hostname: H, redirect_to: R) -> Self
    where
        H: ToString,
        R: Into<Reference<'static>>,
    {
        Config {
            hostname: hostname.to_string(),
            redirect_to: redirect_to.into(),
        }
    }
}

#[doc(hidden)]
#[derive(FromForm)]
pub struct Login<'r> {
    s: &'r str,
}

/// Handles logins.
///
/// Put this into your routes.
#[post("/.well-known/openpgp/login", data = "<login>")]
pub async fn login(login: Form<Login<'_>>,
                   auth: Auth<'_>,
                   config: &State<Config>)
                   -> Result<content::Html<String>, Unauthorized<String>>
{
     do_login(login.s, auth, &config.hostname)
        .await
        .map_err(|e| Unauthorized(Some(e.to_string())))?;

    // Yuck.  Doing a proper redirect doesn't seem to work because
    // browsers won't send the cookie with the redirected request.
    Ok(content::Html(
        format!(r#"<meta http-equiv="refresh" content="0; url={}">"#,
                config.redirect_to)))
}

async fn do_login(s: &str, auth: Auth<'_>, hostname: &str)
                  -> anyhow::Result<String>
{
    let user = authenticate(s, hostname)?;

    if auth.users.get_by_email(&user).await.is_err() {
        auth.users.create_user(&user, "", false).await?;
    }
    auth.login(&fake::Login::new(&user)?).await?;
    Ok(user)
}


/// Handles logouts.
///
/// Put this into your routes.
#[get("/.well-known/openpgp/logout")]
pub fn logout(auth: Auth<'_>, config: &State<Config>)
          -> Result<Redirect, Unauthorized<String>>
{
    auth.logout()
        .map_err(|e| Unauthorized(Some(e.to_string())))?;
    Ok(Redirect::to(config.redirect_to.clone()))
}
