#[macro_use] extern crate rocket;
use rocket::{
    response::{
        content,
    },
};

use rocket_auth::{
    User,
    Users,
};

#[get("/")]
fn index(user: Option<User>) -> content::Html<String> {
    content::Html(
        if let Some(user) = user {
            format!("Hello {}, enjoy your stay.  \
                     Click <a href={}>here</a> to log out.",
                    user.email(),
                    uri!(sequoia_rocket_auth::logout))
        } else {
            "Please log in using:
<pre>
$ sq key generate -u alice@example.org --export key
$ sq-login --key key localhost:8000
</pre>
".into()
        }
    )
}

#[launch]
fn rocket() -> _ {
    use sequoia_rocket_auth::{Config, login, logout};

    let users = Users::open_rusqlite("auth.db").unwrap();

    rocket::build()
        .manage(Config::new("localhost:8000", uri!(index)))
        .mount("/", routes![index, login, logout])
        .manage(users)
}
