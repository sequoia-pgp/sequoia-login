use anyhow::Result;

use sequoia_openpgp::parse::Parse;
use sequoia_openpgp::policy::StandardPolicy;
use sequoia_openpgp::Cert;

use sequoia_auth::create_token;

use rpassword;
use openpgp_card_pcsc::PcscClient;
use openpgp_card_sequoia::card;

use std::path::PathBuf;
use structopt::StructOpt;

use std::fs::File;
use std::io::Write;
use std::time::Duration;
use tempfile::tempdir;

pub const SP: &StandardPolicy<'static> = &StandardPolicy::new();

#[derive(Debug, StructOpt)]
#[structopt(name = "example", about = "An example of StructOpt usage.")]
struct Opt {
    /// The key
    #[structopt(short, long)]
    key: PathBuf,

    /// The website to login with
    hostname: String,
}

fn main() -> Result<()> {
    let opt = Opt::from_args();

    let key_bytes = std::fs::read(opt.key)?;
    let cert = Cert::from_bytes(&key_bytes)?;

    let token =
        if cert.is_tsk() {
            let mut signer = cert
                     .with_policy(SP, None)?
                     .keys()
                     .for_signing() // XXX: for_authentication
                     .next()
                     .ok_or(anyhow::anyhow!("No signing key found"))?
                     .key()
                     .clone()
                     .parts_into_secret()?
                     .into_keypair()?;
            create_token(cert, &mut signer, &opt.hostname)?
        } else {
            let mut card = PcscClient::cards()?.pop().expect("no card found")
                .into();

            // Open card via PCSC
            let mut open = card::Open::new(&mut card)?;

            // Get authorization for signing access to the card with password
            let pin = rpassword::read_password_from_tty(Some("Enter user PIN> "))?;
            open.verify_user_for_signing(&pin)?;
            let mut user = open.signing_card().expect("This should not fail");

            // Get signer (`cert` must contain a public key that corresponds
            // to the key material on the card)
            let mut signer = user.signer(&cert)?;
            create_token(cert, &mut signer, &opt.hostname)?
        };

    // XXX: should be action="https://{}/login
    let sendme = format!(r#"
<form method=post action="http://{}/.well-known/openpgp/login">
  <textarea name=s style="display: none">
{}
  </textarea>
  <noscript>
    <button>Click to login</button>
  </noscript>
</form>
<script>window.onload = () => document.forms[0].submit();</script>
"#, opt.hostname, token);

    // Create a directory inside of `std::env::temp_dir()`.
    let dir = tempdir()?;
    let file_path = dir.path().join("my-temporary-note.html");
    let mut file = File::create(&file_path)?;
    writeln!(file, "{}", sendme)?;

    eprintln!("Your browser is logging in to {}.", opt.hostname);
    open::that(file_path.into_os_string())?;
    std::thread::sleep(Duration::new(1, 0));
    Ok(())
}
