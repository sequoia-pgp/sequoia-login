# Sequoia login - OpenPGP Authentication for Web Applications

This is an easy to use authentication scheme for web applications
using OpenPGP.

## Preliminaries

We assume that the user Juliet has an OpenPGP key with authentication
subkey.  Further, we assume that the web application running on
`example.org` is using this authentication mechanism.

## Login procedure

To log into `example.org`, Juliet creates the following signature:

- The signature is a standalone signature.
- The signature is set to expire in a short time (10s).
- The signature carries the following notations:
  - login-hostname@notations.sequoia-pgp.org: `example.org`
  - issuer-cert@notations.sequoia-pgp.org: Juliet's cert

She then ASCII-armors the signature, and makes a POST request to
`https://example.org/.well-known/openpgp/login`, with a form-encoded
request body mapping `s` to the armored signature.

Upon receiving an authentication request, the server:

- Parses the signature from the POST request.
- Verifies that the signature passes the policy.
- Verifies that the signature is alive (i.e. not yet expired).
- Verifies that the signature's login-hostname matches its hostname.
- Extracts the issuers cert and uses it to verify the signature.
